﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MCSManagement.Startup))]
namespace MCSManagement
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
